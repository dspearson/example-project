(defproject example-project "master-SNAPSHOT"
  :description "example webapp"
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [ring "1.8.0"]
                 [ring/ring-json "0.4.0"]
                 [javax.servlet/servlet-api "2.5"]
                 [aleph "0.4.7-alpha5"]
                 [selmer "1.12.12"]
                 [bidi "2.1.6"]
                 [prone "2019-07-08"]
                 [byte-streams "0.2.4"]
                 [com.taoensso/nippy "2.14.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [org.clojure/java.jdbc "0.7.9"]
                 [org.xerial/sqlite-jdbc "3.28.0"]
                 [com.layerware/hugsql "0.4.9"]
                 [clj-time "0.15.1"]
                 [manifold "0.1.9-alpha3"]
                 [caesium "0.12.0"]
                 [buddy "2.0.0"]]
  :plugins [[cider/cider-nrepl "0.22.0"]
            [lein-ring "0.12.5"]]
  :ring {:handler example-project.core/app}
  :main ^:skip-aot example-project.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
