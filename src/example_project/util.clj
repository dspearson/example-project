(ns example-project.util)

(defn parse-int
  [s]
  (try (Integer. (re-find  #"^\d+$" s)) (catch Exception e false)))
