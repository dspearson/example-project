(ns example-project.core
  (:require [ring.adapter.jetty :as jetty] ;; web server

            ;; internal imports
            [example-project.util :refer :all]
            [example-project.routes :as r]

            ;; better error reporting
            [prone.middleware :as prone]

            ;; routing library
            [bidi.bidi :refer :all]
            [bidi.ring :refer (make-handler)]

            ;; ring middleware for the web server
            [ring.middleware.cookies :refer :all]
            [ring.middleware.params :refer :all]
            [ring.middleware.keyword-params :refer :all]
            [ring.middleware.json :refer :all]
            [ring.middleware.reload :refer :all]
            [ring.util.response :refer :all])
  (:gen-class))

(def entrypoint (make-handler r/route-map))

(def app
  (-> entrypoint
      prone/wrap-exceptions
      wrap-reload
      wrap-keyword-params
      wrap-json-params
      wrap-json-response
      wrap-cookies
      wrap-params))

(defonce server (jetty/run-jetty #'app {:port 3000 :join? false}))

(defn -main
  [& args]

  (while true
    (print "> ")
    (flush)
    (let [command (read-line)]
      (case command
        "exit" (System/exit 0)
        (println "Invalid command!")))))
