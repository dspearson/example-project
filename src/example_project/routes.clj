(ns example-project.routes
  (:require [ring.util.response :refer :all]
            [selmer.parser :as p]))

(defn access-denied
  [body]
  {:status 403 :headers {} :body body})

(defn illegal-route
  [request]
  (not-found "no route found."))

(defn index-handler
  [request]
  (response "example-project"))

(defn info-handler
  [request]
  (let [hostname (.. java.net.InetAddress getLocalHost getHostName)
        current-time (str (new java.util.Date))]
    (response (p/render-file "info.html" {:hostname hostname
                                          :current-time current-time}))))

(def route-map ["/" [["" index-handler]
                     ["info" info-handler]
                     [true illegal-route]]])
